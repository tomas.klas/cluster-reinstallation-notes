# Cluster Reinstallation Notes

This holds some useful information and some commands that may help everyone in the future.


## LXD

The LXD containers can be migrated between node1 and node2 with these commands:

```
# Run this on node1
lxc config set core.https_address <node1 IP>
lxc config set core.trust_password "password"

# And same on the node2
lxc config set core.https_address <node2 IP>
lxc config set core.trust_password "password"

# On node2 add remote target
lxc remote add node1 <node1 IP>
lxc copy <container-from-node2> node1:<container-name-on-other-node>
```

Backup of the container into tarball is done by publishing and exporting:

```
# Export image to local directory
lxc publish <container-name> --alias my-export
lxc image export my-export .

# copy the tarball to desired destination and export it there
scp <tarball> <destination>

# Import after migration of the container
lxc image import <tar-ball> --alias my-export
lxc init my-export <new-container-name>
```

If you import a container it will start in a default network configuration.

We have datamole configuration in a datamole profile, you may be forced to run 

```
# https://manpages.ubuntu.com/manpages/artful/man1/lxc.profile.1.html
lxc profile assign [<remote>:]<container> <profiles>
```

## Docker

If you need to backup some environments for python programmer:

```
# Backup
docker run --rm -it --volume conda_envs:/volume --volume $(pwd):$(pwd) --workdir $(pwd) alpine:latest /bin/sh -c "tar -czf 20200812_conda_envs.tgz /volume"

# Restore
docker run --rm -it --volume conda_envs:/volume --volume $(pwd):$(pwd) --workdir $(pwd) alpine:latest /bin/sh -c "cd / ; tar -xzf $(pwd)/20200812_conda_envs.tgz"
```

If you wish to delete all docker containers: 

```
docker rm $(docker ps -aq)
```

Delete all docker images:

```
docker rmi $(docker images -aq)
```


### Backup & restore envs


```
# Backup the envs
docker run --rm -it --volume conda_envs:/volume --volume $(pwd):$(pwd) --workdir $(pwd) alpine:latest /bin/sh -c "tar -czf 20200812_conda_envs.tgz /volume"

# Restore envs
docker run --rm -it --volume conda_envs:/volume --volume $(pwd):$(pwd) --workdir $(pwd) alpine:latest /bin/sh -c "cd / ; tar -xzf $(pwd)/20200812_conda_envs.tgz"
```


## Reinstallation setps for backup


1. Process of stoping and changing the name:

```
# stop the container
lxc stop <container>

# rename
lxc move <container> (BAKUP|DELETE|MIGRATE|EXPORT)-container
```

2. Backup the config:

```
cd /var/lib/lxd/containers

for i in `ls`; do 
    echo "========${i}==========" >> /mnt/nfsshare/contianer-backups/LXDConfigs
    lxc config show ${i} >> /mnt/nfsshare/contianer-backups/LXDConfigs
done
```

3. Backup the containers for Backup

```
cd /var/lib/lxd/containers

for i in `ls | grep BACKUP`; do 
    echo "========${i}==========" 
    tar -cvzf $(date +%Y%m%d)_${i}.tar.gz /var/lib/lxd/containers/${i}/rootfs/opt /var/lib/lxd/containers/${i}/rootfs/home /var/lib/lxd/containers/${i}/rootfs/root
done
```


## BTRFS 

Adding a device into a btrfs filesystem that is mounted.

```
# adding a device
btrfs device add /dev/sd<?> /

# balancing the fs
btrfs balance start -dconvert=raid1 -mconvert=raid1 /
```


## Test the drives

Testing the drives with smartctl

```
# install smartmontools
apt install smartmontools

# run long test
smartctl -t long /dev/sd<?>
```


## Issues information

After the reinstallation there is sudo in a version that cries out errors into to the stdout when u use it.
It is issue just in contnainers when sudo is trying to change some limits. 
Update the system to fix this issue.

Docker containers inside the LXD exceeds some quotas on disk. 
Based on the discussion I've incrased the number in /proc/sys/kernel/keys/maxkeys.

https://discuss.linuxcontainers.org/t/error-with-docker-inside-lxc-container/922/3
